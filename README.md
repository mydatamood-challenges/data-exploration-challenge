# Survey Data Processing Challenge 

At MyDataMood we are engaged in collecting and understanding user patterns and trends. On that purpose, we want to build a system capable of collecting survey data, process it, and store it in our datawarehouse for later analysis.

On the scope of this code challenge, you will be tasked to code a distributed computing job in [Apache Spark](https://spark.apache.org/), [Apache Beam](https://beam.apache.org/) or similar, to extract certain useful information out of datasets of demographic surveys. 

Taking this dataset as a sample of what kind of data the pipeline should process, [ESS9e03.0_F1.csv.gz](./ESS9e03.0_F1.csv.gz), we need you to develop a pipeline that, based on a file like the one in the sample, calculates and stores the following data:

* On average, and per country, how much time (in minutes) does the people spend using the internet on a daily basis?
* Overall, what is the percentage of people under 35 that ever had a paid job?
* Considering people's net income is distributed amongst 10 categories(from 1 to 10, being 1 the lowest income and 10 the highest), extract the education level most represented on each category.

The destination of these results is not relevant to the challenge. Consider using any format and storage you are familiar with.

Please go through the [Dataset Documentation](./ESS9e03.0.pdf) to get better details on what is held, and what data pieces may better fit your calculations.

## Deliverables
* The application code, and tooling to run it in a production cluster.
* Provide a simple diagram of the cluster architecture needed to run the job in any cloud you like. Consider two possible scenarios: batch processing and streaming processing.

## Evaluation Notes
* You should submit a working application.
* **The application should be production-ready.** Please code it as you would do for something that must go live, and that must be run periodically, and for each new file. 
* Provide all the necessary tooling to run the app on a cluster of your choice.
* General software engineering patterns/good practices will be evaluated.

__NOTE:__ This is not intended to be an Spark/Beam exercise. It's more interesting for us to see your style and how you solve some problems. Even an incorrect result does not mean the challenge will be rejected. Please, be thorough explaining your assumptions.

The the sample data for this exercise was gathered from the [European Social Survey](https://www.europeansocialsurvey.org/) public database.

